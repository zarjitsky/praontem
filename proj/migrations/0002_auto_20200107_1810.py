# Generated by Django 2.2.6 on 2020-01-07 18:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proj', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='documentos',
            options={'verbose_name': 'Documento', 'verbose_name_plural': 'Documentos'},
        ),
        migrations.AlterModelOptions(
            name='empreendimento',
            options={'verbose_name': 'Empreedimento', 'verbose_name_plural': 'Empreendimentos'},
        ),
        migrations.AlterModelOptions(
            name='local',
            options={'verbose_name': 'Local', 'verbose_name_plural': 'Locais'},
        ),
        migrations.RemoveField(
            model_name='empreendimento',
            name='documento',
        ),
        migrations.RemoveField(
            model_name='empreendimento',
            name='id',
        ),
        migrations.RemoveField(
            model_name='local',
            name='empreendimento',
        ),
        migrations.AddField(
            model_name='empreendimento',
            name='ID_C',
            field=models.AutoField(default=0, primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='documentos',
            name='data',
            field=models.DateField(verbose_name='Data'),
        ),
        migrations.AlterField(
            model_name='documentos',
            name='documento',
            field=models.FileField(upload_to='Documentos', verbose_name='Documento'),
        ),
        migrations.AlterField(
            model_name='empreendimento',
            name='cliente',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='proj.cliente'),
        ),
        migrations.AlterField(
            model_name='empreendimento',
            name='cnpj',
            field=models.CharField(max_length=14, verbose_name='CNPJ'),
        ),
        migrations.AlterField(
            model_name='empreendimento',
            name='endereco',
            field=models.CharField(max_length=200, verbose_name='Endereço'),
        ),
        migrations.AlterField(
            model_name='local',
            name='alvara',
            field=models.FileField(upload_to='Alvaras', verbose_name='Alvará'),
        ),
        migrations.AlterField(
            model_name='local',
            name='iptuData',
            field=models.DateField(verbose_name='Data de vencimento'),
        ),
        migrations.AlterField(
            model_name='local',
            name='iptuValor',
            field=models.FloatField(verbose_name='Valor do IPTU'),
        ),
        migrations.AlterField(
            model_name='local',
            name='numeroLocal',
            field=models.IntegerField(verbose_name='Numero do RGI'),
        ),
        migrations.AlterField(
            model_name='local',
            name='rgi',
            field=models.FileField(upload_to='Rgis', verbose_name='RGI'),
        ),
    ]
