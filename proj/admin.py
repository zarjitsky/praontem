from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(cliente)
admin.site.register(empreendimento)
admin.site.register(local)