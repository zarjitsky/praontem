from django.db import models

# Create your models here.


class cliente(models.Model):
    nome = models.CharField(max_length = 100)

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __str__ (self):
        return (self.nome)

class empreendimento(models.Model):
    cliente = models.ForeignKey("cliente", on_delete = models.CASCADE, null=True, blank=True)
    endereco = models.CharField(max_length = 200, verbose_name = "Endereço")
    cnpj = models.CharField(max_length = 14, verbose_name = "CNPJ")
    #local = models.ForeignKey("local", on_delete = models.CASCADE, null=True, blank=True)
    ID_C = models.AutoField(primary_key = True)

    def __str__ (self):
        return (self.endereco)

    class Meta:
        verbose_name = "Empreedimento"
        verbose_name_plural = "Empreendimentos"


class local(models.Model):
    empreendimento = models.ForeignKey("empreendimento", verbose_name = "Empreendimento", on_delete = models.CASCADE, null=True, blank=True)
    iptuValor = models.FloatField(verbose_name = "Valor do IPTU")
    iptuData = models.DateField(verbose_name = "Data de vencimento")
    rgi = models.FileField(upload_to = "Rgis", verbose_name = "RGI")
    numeroLocal = models.IntegerField(verbose_name = "Numero do RGI")
    alvara = models.FileField(upload_to = "Alvaras", verbose_name = "Alvará")

    def __str__(self):
        return(self.empreendimento.endereco)

    class Meta:
        verbose_name = "Local"
        verbose_name_plural = "Locais"

class documentos(models.Model):
    nome = models.CharField(max_length = 200)
    data = models.DateField(verbose_name = "Data")
    documento = models.FileField(upload_to = "Documentos", verbose_name = "Documento")

    def __str__(self):
        return(self.nome)

    class Meta:
        verbose_name = "Documento"
        verbose_name_plural = "Documentos"
